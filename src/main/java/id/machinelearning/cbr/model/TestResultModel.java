package id.machinelearning.cbr.model;

import javax.persistence.*;

@Entity
@Table(name = "t_test_result")
public class TestResultModel {

    @Id
    @GeneratedValue
    @Column(name = "No")
    private Integer no;

    @Column(name = "NoDataset")
    private Integer noDataset;

    @Column(name = "PredictionAcademicSuccess")
    private String predictionAcademicSuccess;

    @Column(name = "Similarity")
    private Double similarity;


    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public Integer getNoDataset() {
        return noDataset;
    }

    public void setNoDataset(Integer noDataset) {
        this.noDataset = noDataset;
    }

    public String getPredictionAcademicSuccess() {
        return predictionAcademicSuccess;
    }

    public void setPredictionAcademicSuccess(String predictionAcademicSuccess) {
        this.predictionAcademicSuccess = predictionAcademicSuccess;
    }

    public Double getSimilarity() {
        return similarity;
    }

    public void setSimilarity(Double similarity) {
        this.similarity = similarity;
    }
}
