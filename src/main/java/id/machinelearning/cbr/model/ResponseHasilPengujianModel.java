package id.machinelearning.cbr.model;

public class ResponseHasilPengujianModel {

    private Double similarity;
    private String predictionAcademicSuccess;

    public Double getSimilarity() {
        return similarity;
    }

    public void setSimilarity(Double similarity) {
        this.similarity = similarity;
    }

    public String getPredictionAcademicSuccess() {
        return predictionAcademicSuccess;
    }

    public void setPredictionAcademicSuccess(String predictionAcademicSuccess) {
        this.predictionAcademicSuccess = predictionAcademicSuccess;
    }

}
