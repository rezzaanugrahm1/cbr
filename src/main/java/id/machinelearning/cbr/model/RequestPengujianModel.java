package id.machinelearning.cbr.model;

public class RequestPengujianModel {

    private String gender;
    private String relation;
    private String parentAnswerSurvey;
    private String parentsSchoolStatisfaction;
    private String studentAbsenceDays;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getParentAnswerSurvey() {
        return parentAnswerSurvey;
    }

    public void setParentAnswerSurvey(String parentAnswerSurvey) {
        this.parentAnswerSurvey = parentAnswerSurvey;
    }

    public String getParentsSchoolStatisfaction() {
        return parentsSchoolStatisfaction;
    }

    public void setParentsSchoolStatisfaction(String parentsSchoolStatisfaction) {
        this.parentsSchoolStatisfaction = parentsSchoolStatisfaction;
    }

    public String getStudentAbsenceDays() {
        return studentAbsenceDays;
    }

    public void setStudentAbsenceDays(String studentAbsenceDays) {
        this.studentAbsenceDays = studentAbsenceDays;
    }
}
