package id.machinelearning.cbr.model;

import javax.persistence.*;

@Entity
@Table(name = "m_dataset")
public class DatasetModel {

    @Id
    @GeneratedValue
    @Column(name = "No")
    private Integer no;

    @Column(name = "Gender")
    private String gender;

    @Column(name = "Relation")
    private String relation;

    @Column(name = "ParentAnswerSurvey")
    private String parentAnswerSurvey;

    @Column(name = "ParentsSchoolStatisfaction")
    private String parentsSchoolStatisfaction;

    @Column(name = "StudentAbsenceDays")
    private String studentAbsenceDays;

    @Column(name = "Class")
    private String className;

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getParentAnswerSurvey() {
        return parentAnswerSurvey;
    }

    public void setParentAnswerSurvey(String parentAnswerSurvey) {
        this.parentAnswerSurvey = parentAnswerSurvey;
    }

    public String getParentsSchoolStatisfaction() {
        return parentsSchoolStatisfaction;
    }

    public void setParentsSchoolStatisfaction(String parentsSchoolStatisfaction) {
        this.parentsSchoolStatisfaction = parentsSchoolStatisfaction;
    }

    public String getStudentAbsenceDays() {
        return studentAbsenceDays;
    }

    public void setStudentAbsenceDays(String studentAbsenceDays) {
        this.studentAbsenceDays = studentAbsenceDays;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
