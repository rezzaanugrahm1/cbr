package id.machinelearning.cbr.controller;

import com.google.gson.Gson;
import id.machinelearning.cbr.model.RoleModel;
import id.machinelearning.cbr.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    RoleRepository roleRepository;

    @PostMapping("/add")
    public String add(@RequestBody RoleModel roleModel ){
        roleRepository.save(roleModel);
        return new Gson().toJson(roleModel);
    }
}
