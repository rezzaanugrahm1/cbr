package id.machinelearning.cbr.controller;

import com.google.gson.Gson;
import id.machinelearning.cbr.model.*;
import id.machinelearning.cbr.repository.DatasetRepository;
import id.machinelearning.cbr.repository.TestResultRepository;
import id.machinelearning.cbr.repository.RoleRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/pengujian")
public class PengujianController {
    public static final Logger logger = LogManager.getLogger(PengujianController.class);

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    DatasetRepository datasetRepository;

    @Autowired
    TestResultRepository historyRepository;

    @Value("${value.weight}")
    private Integer weight;

    @Value("${value.same}")
    private Integer same;

    @Value("${value.notsame}")
    private Integer notsame;

    @PostMapping("/add")
    public String add(@RequestBody RequestPengujianModel dataUji){
        // process cbr
        List<ResponseHasilPengujianModel> responseHasilPengujianModels = pengujian(dataUji);

        // get similarity
        ResponseHasilPengujianModel responseHasilPengujianModel =  getMaxSimilarity(responseHasilPengujianModels);

        // save to db
        saveHistory(dataUji , responseHasilPengujianModel);

        // resulst to user
        return new Gson().toJson(responseHasilPengujianModel);
    }

    private List<ResponseHasilPengujianModel> pengujian(RequestPengujianModel dataUji){
        List<ResponseHasilPengujianModel> responseHasilPengujianModels = new ArrayList<ResponseHasilPengujianModel>();
        List<RoleModel> listRoleModel = roleRepository.findAll();
        listRoleModel.stream().forEach(role->{
            Integer totalWeight = role.getWeight();
            Integer gender = role.getGender()!=null ? role.getGender().toLowerCase().equals(dataUji.getGender().toLowerCase()) ? same*weight : notsame*weight : notsame;
            Integer relation = role.getRelation()!=null ? role.getRelation().toLowerCase().equals(dataUji.getRelation().toLowerCase()) ? same*weight : notsame*weight : notsame;
            Integer parentAnswerSurvey = role.getParentAnswerSurvey()!=null ? role.getParentAnswerSurvey().toLowerCase().equals(dataUji.getParentAnswerSurvey().toLowerCase()) ? same*weight : notsame*weight : notsame;
            Integer parentasSchoolStatisfaction = role.getParentsSchoolStatisfaction()!=null ? role.getParentsSchoolStatisfaction().toLowerCase().equals(dataUji.getParentsSchoolStatisfaction().toLowerCase()) ? same*weight : notsame*weight : notsame;
            Integer studentAbsence = role.getStudentAbsenceDays()!=null ? role.getStudentAbsenceDays().toLowerCase().equals(dataUji.getStudentAbsenceDays().toLowerCase()) ? same*weight : notsame*weight : notsame;

            Double similairty =   Double.valueOf ((gender + relation + parentAnswerSurvey + parentasSchoolStatisfaction + studentAbsence ))  /  Double.valueOf(totalWeight);
            logger.info("No Role "+ role.getNo()     + " _ similarity : " + similairty);

            ResponseHasilPengujianModel pengujianModel = new ResponseHasilPengujianModel();
            pengujianModel.setSimilarity(similairty);
            pengujianModel.setPredictionAcademicSuccess(role.getPredictionAcademicSuccess());
            responseHasilPengujianModels.add(pengujianModel);
        });

      return responseHasilPengujianModels;
    }

    private ResponseHasilPengujianModel getMaxSimilarity(List<ResponseHasilPengujianModel> responseHasilPengujianModels){
        ResponseHasilPengujianModel responseHasilPengujianModel = responseHasilPengujianModels
                .stream()
                .max(Comparator.comparing(ResponseHasilPengujianModel::getSimilarity))
                .orElseThrow(NoSuchElementException::new);
        return responseHasilPengujianModel;
    }

    private void saveHistory(RequestPengujianModel dataUji , ResponseHasilPengujianModel responseHasilPengujianModel ){
        TestResultModel historyModel = new TestResultModel();
        historyModel.setSimilarity(responseHasilPengujianModel.getSimilarity());
        historyModel.setPredictionAcademicSuccess(responseHasilPengujianModel.getPredictionAcademicSuccess().toUpperCase());

        logger.info("historyModel : " + historyModel.toString());
        historyRepository.save(historyModel);
    }


    @GetMapping({"/publish"})
    public void publish(){
        List<RoleModel> listRoleModel = roleRepository.findAll();
        List<DatasetModel> listDatasetModel = datasetRepository.findAll();

        for(int x =0 ; x < listDatasetModel.size() ; x++){
            DatasetModel dataUji = listDatasetModel.get(x);

            List<ResponseHasilPengujianModel> responseHasilPengujianModels = new ArrayList<ResponseHasilPengujianModel>();
            listRoleModel.stream().forEach(role->{
                Integer totalWeight = role.getWeight();
                Integer gender = role.getGender()!=null ? role.getGender().toLowerCase().equals(dataUji.getGender().toLowerCase()) ? same*weight : notsame*weight : notsame;
                Integer relation = role.getRelation()!=null ? role.getRelation().toLowerCase().equals(dataUji.getRelation().toLowerCase()) ? same*weight : notsame*weight : notsame;
                Integer parentAnswerSurvey = role.getParentAnswerSurvey()!=null ? role.getParentAnswerSurvey().toLowerCase().equals(dataUji.getParentAnswerSurvey().toLowerCase()) ? same*weight : notsame*weight : notsame;
                Integer parentasSchoolStatisfaction = role.getParentsSchoolStatisfaction()!=null ? role.getParentsSchoolStatisfaction().toLowerCase().equals(dataUji.getParentsSchoolStatisfaction().toLowerCase()) ? same*weight : notsame*weight : notsame;
                Integer studentAbsence = role.getStudentAbsenceDays()!=null ? role.getStudentAbsenceDays().toLowerCase().equals(dataUji.getStudentAbsenceDays().toLowerCase()) ? same*weight : notsame*weight : notsame;

                Double similairty =   Double.valueOf ((gender + relation + parentAnswerSurvey + parentasSchoolStatisfaction + studentAbsence ))  /  Double.valueOf(totalWeight);
                ResponseHasilPengujianModel pengujianModel = new ResponseHasilPengujianModel();
                pengujianModel.setSimilarity(similairty);
                pengujianModel.setPredictionAcademicSuccess(role.getPredictionAcademicSuccess());
                responseHasilPengujianModels.add(pengujianModel);
            });
            ResponseHasilPengujianModel responseHasilPengujianModel = getMaxSimilarity(responseHasilPengujianModels);
            logger.info("responseHasilPengujianModel : " + dataUji.getNo() + " : "+ responseHasilPengujianModel.getSimilarity()+ " : "+ responseHasilPengujianModel.getPredictionAcademicSuccess());
            saveTestResult(dataUji , responseHasilPengujianModel);
        }
    }

    private void saveTestResult(DatasetModel datasetModel , ResponseHasilPengujianModel responseHasilPengujianModel){
        TestResultModel historyModel = new TestResultModel();
        historyModel.setNoDataset(datasetModel.getNo());
        historyModel.setSimilarity(responseHasilPengujianModel.getSimilarity());
        historyModel.setPredictionAcademicSuccess(responseHasilPengujianModel.getPredictionAcademicSuccess().toUpperCase());
        historyRepository.save(historyModel);
    }

}
