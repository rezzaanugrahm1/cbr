package id.machinelearning.cbr.repository;

import id.machinelearning.cbr.model.DatasetModel;
import id.machinelearning.cbr.model.RoleModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DatasetRepository extends JpaRepository<DatasetModel, Long> {
    RoleModel findByNo(Integer nomor);

}
