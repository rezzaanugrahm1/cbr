package id.machinelearning.cbr.repository;

import id.machinelearning.cbr.model.RoleModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<RoleModel, Long> {
    RoleModel findByNo(Integer nomor);

}
