package id.machinelearning.cbr.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.ArrayList;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactory",
        transactionManagerRef = "postgreTransactionManager", basePackages = {"id.machinelearning.cbr.repository"})
public class DbConfig {
        private String[] postgreJpaPackages;

        @Bean(name = "ds")
        @ConfigurationProperties(prefix = "spring.datasource")
        public DataSource dataSource(DataSourceProperties properties) {
            return properties.initializeDataSourceBuilder()
                    .type(HikariDataSource.class).build();
        }

        @Bean(name = "entityManagerFactory")
        public LocalContainerEntityManagerFactoryBean barEntityManagerFactory(
                EntityManagerFactoryBuilder builder, @Qualifier("ds") DataSource dataSource) {
            return builder.dataSource(dataSource).packages(this.getJpaPackages()).persistenceUnit("postgrePU")
                    .build();
        }

        @Bean(name = "postgreTransactionManager")
        public PlatformTransactionManager barTransactionManager(
                @Qualifier("entityManagerFactory") EntityManagerFactory barEntityManagerFactory) {
            return new JpaTransactionManager(barEntityManagerFactory);
        }

        final String[] getJpaPackages() {
            ArrayList<String> arrayList = new ArrayList<>();
            arrayList.add("id.machinelearning.cbr.model");
            postgreJpaPackages = new String[arrayList.size()];
            arrayList.toArray(postgreJpaPackages);
            return postgreJpaPackages;
        }
}
